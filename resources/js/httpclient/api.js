import axios from 'axios'

const api = axios.create({
    baseURL: 'https://app.timecamp.com/third_party/api',
    headers: {
        Authorization: 'Bearer fbe3ace124996ca83061e2715b'
    }
})

// Add a request interceptor
api.interceptors.request.use(function (config) {
    // Do something before request is sent
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

// Add a response interceptor
api.interceptors.response.use(function (response) {
    // Do something with response data
    return response;
}, function (error) {
    // Do something with response error
    return Promise.reject(error);
});

export { api }
