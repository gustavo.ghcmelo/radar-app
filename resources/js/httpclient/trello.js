import axios from 'axios'

const apiTrello = axios.create({
    baseURL: 'https://api.trello.com'
})

// Add a request interceptor
apiTrello.interceptors.request.use(function (config) {
    // Do something before request is sent
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

// Add a response interceptor
apiTrello.interceptors.response.use(function (response) {
    // Do something with response data
    return response;
}, function (error) {
    // Do something with response error
    return Promise.reject(error);
});

export { apiTrello }
